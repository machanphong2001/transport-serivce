package com.reeco.transportservice.connection;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import com.reeco.transportservice.connection.dto.CreateConnectionDto;
import com.reeco.transportservice.connection.dto.UpdateConnectionDto;
import com.reeco.transportservice.shared.dto.ResponseData;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/connection")
public class ConnectionController {
  // CRUD
  @Autowired
  private ConnectionService connectionService;

  @PostMapping
  public DeferredResult<ResponseEntity<Object>> createConnection(@RequestBody CreateConnectionDto createConnectionDto) {
    DeferredResult<ResponseEntity<Object>> deferredResult = new DeferredResult<>();
    Connection connection = connectionService.createConnection(createConnectionDto);
    deferredResult.setResult(new ResponseEntity<>(new ResponseData<>(HttpStatus.CREATED.value(), "Connection created successfully", connection), HttpStatus.CREATED));
    return deferredResult;
  }

  @GetMapping
  public ResponseEntity<Object> getAllConnections() {
    List<Connection> connections = connectionService.getAllConnections();
    return new ResponseEntity<>(new ResponseData<>(HttpStatus.OK.value(), "Connections retrieved successfully", connections), HttpStatus.OK);
  }

  @GetMapping(path = "/{id}")
  public ResponseEntity<Object> getConnectionById(@PathVariable(name = "id") Long id) {
    Connection result = connectionService.getConnectionById(id);
    return new ResponseEntity<>(new ResponseData<>(HttpStatus.OK.value(), "Connection retrieved successfully", result), HttpStatus.OK);
  }

  @PutMapping(path = "/{id}")
  public ResponseEntity<Object> updateConnection(@PathVariable(name = "id") Long id, @RequestBody UpdateConnectionDto connection) {
    Connection result = connectionService.updateConnection(id, connection);
    return new ResponseEntity<>(new ResponseData<>(HttpStatus.OK.value(), "Connection updated successfully", result), HttpStatus.OK);
  }

  @DeleteMapping(path = "/{id}")
  public ResponseEntity<Object> deleteConnection(@PathVariable(name = "id") Long id) {
    connectionService.deleteConnection(id);
    return new ResponseEntity<>(new ResponseData<>(HttpStatus.OK.value(), "Connection deleted successfully", null), HttpStatus.OK);
  }
  
}
