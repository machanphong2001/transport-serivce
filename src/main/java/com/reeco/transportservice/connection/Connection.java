package com.reeco.transportservice.connection;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.reeco.transportservice.connection.dto.CreateConnectionDto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@Entity()
@Table(name = "connections")
@Where(clause = "deleted IS NULL")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class  Connection {
  @Id
  @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
  private Long id;
  
  @Column(name = "name")
  private String name;

  @Column(name = "code")
  private String code;


  @Column(name = "ip_address")
  private String IPAddress;

  @Column(name = "port")
  private String port;

  @Column(name = "protocol")
  private String protocol;

  
  @Column(name = "created_at")
  @CreationTimestamp
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Date createdAt;

  @Column(name = "updated_at")
  @UpdateTimestamp
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Date updatedAt;

  @Column(name = "deleted")
  private Boolean deleted;

  public Connection(CreateConnectionDto connectionDto) {
    this.name = connectionDto.getName();
    this.code = connectionDto.getCode();
    this.IPAddress = connectionDto.getIpAddress();
    this.port = connectionDto.getPort();
    this.protocol = connectionDto.getProtocol();
  }
}
