package com.reeco.transportservice.connection.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CreateConnectionDto {
  private String name;
  private String code;
  private String ipAddress;
  private String port;
  private String protocol;
}
