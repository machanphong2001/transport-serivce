package com.reeco.transportservice.connection;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.reeco.transportservice.connection.dto.CreateConnectionDto;
import com.reeco.transportservice.connection.dto.UpdateConnectionDto;
import com.reeco.transportservice.shared.exceptions.NotFoundException;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ConnectionService {

  private final ConnectionRepository connectionRepository;
  
  // @Transactional
  public Connection createConnection(CreateConnectionDto connectionDto) {
    Connection connection = new Connection(connectionDto);
    return connectionRepository.save(connection);
  }

  public List<Connection> getAllConnections() {
    List<Connection> connections = connectionRepository.findAll();
        
    return connections;
  }

  public Connection getConnectionById(Long id) {
    if (id == null) {
      throw new NotFoundException("Connection not found");
    }
    Connection connection = connectionRepository.findById(id).orElse(null);

    if (connection == null) {
      throw new NotFoundException("Connection not found");
    }
    return connection;
  }

  @SuppressWarnings("null")
  public Connection updateConnection(Long id, UpdateConnectionDto connection) {
    
    Connection connectionToUpdate = connectionRepository.findById(id).orElse(null);
    if (connectionToUpdate == null) {
      return null;
    }
    connectionToUpdate.setName(connection.getName());
    connectionToUpdate.setCode(connection.getCode());
    connectionToUpdate.setIPAddress(connection.getIpAddress());
    connectionToUpdate.setPort(connection.getPort());
    connectionToUpdate.setProtocol(connection.getProtocol());
    return connectionRepository.save(connectionToUpdate);
  }

  @SuppressWarnings("null")
  public void deleteConnection(Long id) {
    Connection connection = connectionRepository.findById(id).orElse(null);
    if (connection != null) {
      connection.setDeleted(true);
      connectionRepository.save(connection);
    }
  }

}
