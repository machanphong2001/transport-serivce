package com.reeco.transportservice.connection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ConnectionRepository
 */
@Repository
public interface ConnectionRepository extends JpaRepository<Connection, Long>{


}
